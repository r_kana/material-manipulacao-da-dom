import PRODUCTS from './assets/data/products.js'

// Constante que representa o carrinho de compras em memória
const CART = []

// Funções utilizados para abstrair as requisições ao banco de dados

const findProductWithId = id => {
  for(let i = 0; i < PRODUCTS.length; i++) {if (PRODUCTS[i].id == id) return PRODUCTS[i]; }
}

const hasInStock = product => product.stock > 0


const cartProduct = product => {
  return {
    "id": product.id,
    "name": product.name,
    "color": product.color,
    "quantity": 1,
    "totalPrice": product.price
  }
}

const addToStock = item => {
  PRODUCTS.find(product => product.id == item.id).stock += item.quantity;
}

const updateCartOnRemove = cartElement => {
  let index;
  const newCart = []
  for (let i = 0; i < cartElement.children.length; i++) {
    const productIndex = PRODUCTS.findIndex(element => element.id == cartElement.children[i].id);
    newCart.push(cartProduct(PRODUCTS[productIndex]));
  }

  for(let i = 0; i < newCart.length ; i++) {
    if(CART[i].id !== newCart[i].id) {
      index = i;
      break;
    }
  }
  if(index === undefined) { index = CART.length - 1 }

  addToStock(CART[index]);

  CART.splice(index, 1);
}

export {
  findProductWithId,
  hasInStock,
  updateCartOnRemove,
  cartProduct,
  CART
}