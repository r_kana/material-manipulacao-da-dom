import PRODUCTS from './assets/data/products.js'
import {
  findProductWithId,
  hasInStock,
  updateCartOnRemove,
  cartProduct,
  CART
} from './abstraction.js'

////////// RENDER //////////
// Funções para estilização e criação dos elementos da DOM

function renderColor() {
  return {
    "Marrom": "rgba(36, 20, 20,0.7)",
    "Azul": "rgba(15, 27, 190, 0.7)",
    "Verde": "rgba(15, 190, 15, 0.747)",
    "Caqui": "rgba(201, 192, 115, 0.7)",
    "Xadrez": "repeating-linear-gradient(to bottom, transparent, transparent 5px, #000307 5px, #7c7d28 10px),repeating-linear-gradient(to right, #e69010, #617ca2 5px, white 5px, #7c7d28 10px)",
    "Jeans": "repeating-linear-gradient(to bottom, transparent, transparent 2px, #28487d 2px, #28487d 3px), repeating-linear-gradient(to right, #617ca2, #617ca2 2px, #28487d 2px, #28487d 3px)",
    "Preto": "rgba(22, 21, 21, 0.904)",
    "Colorido": "linear-gradient(90deg, red, orange, yellow, green, blue, indigo, violet, red)"
  }
}

const renderProducts = (items, target) => {
  const list = document.querySelector(target)
  list.innerHTML = ""

  items.forEach(item => {
    //Criação do container com classe e Id
    const productContainer = document.createElement('div')
    productContainer.classList.add('product-container')
    productContainer.id = item.id

    //Criação e inclusão da imagem com seu container
    const productImage = document.createElement('div')
    productImage.classList.add("product-image")
    const img = document.createElement('img')
    img.src = item.imageUrl
    productImage.appendChild(img)
    productContainer.appendChild(productImage)

    //Criação e inclusão das infos do produto
    const productInfo = document.createElement('div')
    productInfo.classList.add("product-info")
    productInfo.innerHTML = "<h1>" + item.name + "</h1>"
    productInfo.innerHTML += "<p>" + stockSituation(item.stock) + "</p>"
    productInfo.innerHTML += "<p>" + item.description + "</p>"

    //Inclusão do círculo de container
    const colorCircle = document.createElement('div')
    colorCircle.classList.add("color-circle")
    colorCircle.style.background = renderColor()[item.color]
    productInfo.appendChild(colorCircle)

    //Inclusão do preço
    productInfo.innerHTML += "<p class='price'>R$" + item.price + "</p>"

    //Adicionar botão ao info do produto
    const btnAddCart = document.createElement('button')
    btnAddCart.classList.add("button")
    btnAddCart.textContent = "Adicionar"
    btnAddCart.onclick = addItemToCart
    productInfo.appendChild(btnAddCart)

    //Incluir o product-info no container
    productContainer.appendChild(productInfo)

    //Adicionar o container no produto
    list.appendChild(productContainer)
  })
}

function renderCart (cartClass) {
  document.querySelector(cartClass).innerHTML = "<p id='emptyCart'>Carrinho Vazio</p>";
}
////////// FIM RENDER //////////


// Função para mudar a class do modal, mostrando o modal
function showModal(modalId) {
  const modal = document.getElementById(modalId);
  modal.classList.add("show");

  modal.addEventListener('click',(e) => {
    if(e.target.id == modalId || e.target.className == "close"){
      modal.classList.remove("show");
    }
  });
}

// Adiciona item a lista de produtos do carrinho
function addItemToCart (element) {
  const cartElement = document.querySelector(".cart");
  const productId = element.target.parentNode.parentNode.id;
  const product = findProductWithId (productId);
  const index = CART.findIndex(product => product.id == id);

  if(document.getElementById("#emptyCart")) cartElement.innerHTML = ""

  if (hasInStock(product)) {
    product.stock--
    
    if (index !== -1) {
      CART[index].quantity++;
      CART[index].totalPrice += product.price;

      for(let i = 0; i < cartElement.children.length; i++) {
        if (CART[index].id == cartElement.children[i].id) {
          const cartItemParagraph = cartElement.children[i].children[0];
          cartItemParagraph.textContent = `${CART[index].name}(${CART[index].color}) `+
                                          `- x${CART[index].quantity} `+
                                          `- R\$${CART[index].totalPrice.toFixed(2)}`
          break;
        }
      }
    }

    else {
      const productObject = cartProduct(product);
      CART.push(productObject)
      const cartItem = document.createElement('div')
      cartItem.setAttribute ("id", `${productObject.id}`)

      const cartItemParagraph = document.createElement('p');
      cartItemParagraph.classList.add("cart-item-paragraph");
      cartItemParagraph.textContent = `${productObject.name}(${productObject.color}) `+
                                      `- x${productObject.quantity} `+
                                      `- R\$${productObject.totalPrice.toFixed(2)}`

      const button = document.createElement('button');
      button.classList.add("remove-item");
      button.textContent = "X";
      button.addEventListener("click", removeItemFromCart);
      cartItem.appendChild(cartItemParagraph);
      cartItem.appendChild(button);
      cartElement.appendChild(cartItem);
    }
  } 

  else {
    showModal("modal-esgotado");
  }
}

// Remove item a lista de produtos do carrinho
const removeItemFromCart = element => {
  const cartElement = document.querySelector(".cart");
  cartElement.removeChild(element.target.parentNode);

  updateCartOnRemove(cartElement);//abstração

  if (CART.length === 0) {cartElement.innerHTML = "<p id='emptyCart'>Carrinho Vazio</p>"};
}



function filterColor(element) {
  if (element.target.value !== "Todas") {
    const filtered = PRODUCTS.filter( product => product.color === element.target.value)
    renderProducts(filtered, ".product-display")
  } 
  else renderProducts(PRODUCTS, ".product-display")
}



//Fitra os itens por cor
function renderFilter(parent) {
  const filterContainer = document.querySelector(parent)
  const select = document.createElement('select')
  const label = document.createElement('label')
  select.id = 'colors'
  label.setAttribute('for', 'colors')
  label.textContent = "Selecione uma cor: "
  filterContainer.appendChild(label)
  filterContainer.appendChild(select)
  select.classList.add('selection')
  const todas = document.createElement('option')
  todas.setAttribute("value", "Todas")
  todas.textContent = "Todas"
  select.appendChild(todas)
  const colors = Object.keys(renderColor()).sort()
  colors.forEach(item => {
    const option = document.createElement('option')
    option.setAttribute("value", item)
    option.textContent = item
    select.appendChild(option)
  })
  select.onchange = filterColor
}

//Verificar se tem o item em estoque
function stockSituation (stock) {
  if (stock) return "Em estoque"
  else return "Em falta"
}


//Principal
renderFilter(".filter")
renderProducts(PRODUCTS, ".product-display")
renderCart(".cart")

// window.addEventListener('click', removeItemCart)