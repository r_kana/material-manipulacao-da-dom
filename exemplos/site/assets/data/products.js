const PRODUCTS = [
  { id:1,
    name:"Bermuda",
    color:"Preto",
    description:"Uma bermuda preta",
    price:159.99,
    stock: 2,
    imageUrl:"./assets/image/black-bermuda.png"
    },
  {
    id:2,
    name:"Vestido",
    color:"Preto",
    description: "Um vestido preto",
    price: 215.99,
    stock: 10,
    imageUrl:"./assets/image/black-dress.png"
  },
  {
    id:3,
    name: "Calça",
    color:"Preto",
    description:"Uma calça preta",
    price: 129.90,
    stock: 7,
    imageUrl:"./assets/image/black-pants.png"
  },
   {
    id:4,
    name: "Camisa",
    color:"Preto",
    description:"Uma camisa preta",
    price: 129.90,
    stock: 7,
    imageUrl:"./assets/image/black-shirt.png"
  },
   {
    id:5,
    name: "Saia",
    color:"Preto",
    description:"Uma saia preta",
    price: 159.90,
    stock: 5,
    imageUrl:"./assets/image/black-skirt.png"
  },
   {
    id:6,
    name: "Meias",
    color:"Preto",
    description:"Meias pretas",
    price: 129.90,
    stock: 10,
    imageUrl:"./assets/image/black-socket.png"
  },
   {
    id:7,
    name: "Meias",
    color:"Azul",
    description:"Uma meia azul",
    price: 129.90,
    stock: 2,
    imageUrl:"./assets/image/blue-socket.png"
  },
   {
    id:8,
    name: "Vestido",
    color:"Marrom",
    description:"Um vestido marrom",
    price: 229.90,
    stock: 3,
    imageUrl:"./assets/image/brown-dress.png"
  },
   {
    id:9,
    name: "Óculos",
    color:"Marrom",
    description:"Um óculos marrom",
    price: 329.90,
    stock: 1,
    imageUrl:"./assets/image/brown-glasses.png"
  },
   {
    id:10,
    name: "Sapatos",
    color:"Marrom",
    description:"Um par de sapatos marrons",
    price: 129.90,
    stock: 6,
    imageUrl:"./assets/image/brown-shoes.png"
  },
   {
    id:11,
    name: "Vestido",
    color:"Colorido",
    description:"Um vestido colorido",
    price: 159.90,
    stock: 10,
    imageUrl:"./assets/image/colored-dress.png"
  },
   {
    id:12,
    name: "Óculos",
    color:"Colorido",
    description:"Um óculos colorido",
    price: 129.90,
    stock: 8,
    imageUrl:"./assets/image/colored-glasses.png"
  },
   {
    id:13,
    name: "Meias",
    color:"Colorido",
    description:"Um par de meias coloridas",
    price: 29.90,
    stock: 7,
    imageUrl:"./assets/image/colored-socket.png"
  },
   {
    id:14,
    name: "Saia",
    color:"Verde",
    description:"Uma saia verde",
    price: 239.90,
    stock: 3,
    imageUrl:"./assets/image/green-skirt.png"
  },
   {
    id:15,
    name: "Bermuda",
    color:"Jeans",
    description:"Uma bermuda jeans",
    price: 179.90,
    stock: 5,
    imageUrl:"./assets/image/jeans-bermuda.png"
  },
   {
    id:16,
    name: "Calça",
    color:"Jeans",
    description:"Uma calça jeans",
    price: 189.90,
    stock: 7,
    imageUrl:"./assets/image/jeans-pants.png"
  },
     {
    id:17,
    name: "Camisa",
    color:"Jeans",
    description:"Uma camisa jeans",
    price: 109.90,
    stock: 1,
    imageUrl:"./assets/image/jeans-shirt.png"
  },
     {
    id:18,
    name: "Calça",
    color:"Caqui",
    description:"Uma calça caqui",
    price: 129.90,
    stock: 3,
    imageUrl:"./assets/image/khaki-pants.png"
  },
     {
    id:19,
    name: "Camisa",
    color:"Xadrez",
    description:"Uma camisa xadrez",
    price: 129.90,
    stock: 7,
    imageUrl:"./assets/image/plaid-shirt.png"
  },
     {
    id:20,
    name: "Sapatos",
    color:"Colorido",
    description:"Um par de sapatos coloridos",
    price: 129.90,
    stock: 7,
    imageUrl:"./assets/image/colored-shoes.png"
  },

]

export default PRODUCTS