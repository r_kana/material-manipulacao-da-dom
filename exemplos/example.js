// ------ SELETORES ------ 

// >> querySelector
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector
//  - Exemplo:
// const body = document.querySelector("body");
// const cartClass = document.querySelector(".cart");
// const cartId = document.querySelector("#meu-id");

// >> getElementByClassName
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Element/getElementsByClassName
//  - Exemplo:
// const cartClass = document.getElementsByClassName("cart");

// >> getElementById
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById
//  - Exemplo:
// const cartId = document.getElementById("meuId");

// >> children
//  - Docs: https://developer.mozilla.org/en-US/docs/Web/API/Element/children
//  - Exemplo: 
// const avo = document.getElementById("avo");
// const avoChildren = avo.children

// >> firstElementChild e lastElementChild
//  - Doc 1: https://developer.mozilla.org/en-US/docs/Web/API/Element/firstElementChild
//  - Doc 2: https://developer.mozilla.org/en-US/docs/Web/API/Element/lastElementChild
//  - Exemplo:
// const pai = avo.firstElementChild;
// const tio = avo.lastElementChild;

// >> parentNode
//  - Doc:https://developer.mozilla.org/en-US/docs/Web/API/Node/parentNode
//  - Exemplo:
// const filho = document.getElementById("filho");
// const pai = filho.parentNode;
// const avo = pai.parentNode;
// const avo = filho.parentNode.parentNode;


// ------ CRIAÇÂO DE ELEMENTOS ------ 

// >> createElement
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement
//  - Exemplo:
// const avo = document.querySelector("#avo");
// const pai = document.createElement("div");
// const tio = document.createElement("div");
// const filho = document.createElement("div");


// ------ DELEÇÂO DE ELEMENTOS ------ 
// >> remove
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Element/remove
//  - Exemplo:
// const avo = document.querySelector("#avo");
// avo.remove();


// ------ MANIPULAÇÃO DE PROPRIEDADES ------ 

// >> innerText
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/innerText
//  - Exemplo:
// avo.innerText = "AVÔ"
// pai.innerText = "PAI"
// tio.innerText = "TIO"
// filho.innerText = "FILHO"

// >> id
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/id
//  - Exemplo:
// pai.id = "pai"
// tio.id = "tio"
// filho.id ="filho"

// >> appendChild
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild
//  - Exemplo:
// avo.appendChild(pai);
// avo.appendChild(tio);
// pai.appendChild(filho);

// >> classList
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
//  - Exemplo:
// avo.classList.toggle("cart")

// >> style
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/style
//  - Exemplo:
// avo.style.backgroundColor = "blue"


// ------ EVENT LISTENERS ------ 

// >> addEventListener
//  - Doc: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
//  - Exemplo 1: 
// const button = document.querySelector("#button");

// button.addEventListener('click', (element) => {
//   const filho = document.createElement("div");
//   filho.classList.add("filho");
//   filho.innerText = "FILHO";
//   avo.appendChild(filho);
// });

// button.onclick = (element) => {
//   const filho = document.createElement("div");
//   filho.classList.add("filho");
//   filho.innerText = "FILHO";
//   avo.appendChild(filho);
// }

//  - Exemplo 2:
// button.addEventListener('click', (event) => {
//   const div = document.createElement("div");
//   div.classList.add("filho");

//   const nome = document.createElement("p");
//   nome.innerText = "FILHO";
  
//   const buttonDelete = document.createElement("button");
//   buttonDelete.innerText = "X";
//   buttonDelete.addEventListener('click', (event) => {
//     event.target.parentNode.remove();
//   });

//   div.appendChild(nome);
//   div.appendChild(buttonDelete);
//   avo.appendChild(div); 
// });

//  - Exemplo 3:
// button.addEventListener('click', (element) => {
//   const div = document.createElement("div");

//   const input = document.createElement("input");
//   input.id = "inputName"
//   input.placeholder = "Nome do Filho"

//   const submitButton = document.createElement("button");
//   submitButton.innerText = "Criar";

//   submitButton.addEventListener("click", (event) => {
//     const parentDiv = event.target.parentNode;

//     const inputName = document.querySelector("#inputName")

//     const p = document.createElement("p");
//     p.innerText = inputName.value;
//     // p.innerText = parentDiv.firstElementChild.value;

//     const buttonDelete = document.createElement("button");
//     buttonDelete.innerText = "X";
//     buttonDelete.addEventListener('click', (event) => {
//       event.target.parentNode.remove();
//     });

//     // parentDiv.innerText = '' ou parentDiv.innerHTML = ''
//     inputName.remove();
//     event.target.remove();

//     parentDiv.appendChild(p);
//     parentDiv.appendChild(buttonDelete);
//   });

//   div.classList.add("filho");

//   // filho.innerText = "FILHO";
//   div.appendChild(input)
//   div.appendChild(submitButton)
//   avo.appendChild(div);
// });
